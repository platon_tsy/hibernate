import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StatusConverter implements AttributeConverter<Status, String> {

    public String convertToDatabaseColumn(Status value) {
        if ( value == null ) {
            return null;
        }

        return value.getCode();
    }

    public Status convertToEntityAttribute(String value) {
        if ( value == null ) {
            return null;
        }

        return Status.fromCode(value);
    }
}