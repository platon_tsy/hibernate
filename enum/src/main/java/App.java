import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Arrays;
import java.util.List;

public class App {

    public static void main (String...args){

        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();

        Activity date = new Activity(Status.ACTIVE, Status.ACTIVE, Status.ACTIVE);

        session.save(date);
        tx.commit();

        List<Activity> activities = session.createQuery("from Activity").list();
        System.out.println(Arrays.toString(activities.toArray()));

        session.close();
        HibernateUtil.shutdown();
    }
}
