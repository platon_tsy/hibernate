import javax.persistence.*;

@Entity
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.ORDINAL)
    private Status ordinal;

    @Enumerated(EnumType.STRING)
    private Status string;

    @Convert(converter = StatusConverter.class)
    private Status converted;

    public Activity() {
    }

    public Activity(Status ordinal, Status string, Status converted) {
        this.ordinal = ordinal;
        this.string = string;
        this.converted = converted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Status getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Status ordinal) {
        this.ordinal = ordinal;
    }

    public Status getString() {
        return string;
    }

    public void setString(Status string) {
        this.string = string;
    }

    public Status getConverted() {
        return converted;
    }

    public void setConverted(Status converted) {
        this.converted = converted;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", ordinal=" + ordinal +
                ", string=" + string +
                ", converted=" + converted +
                '}';
    }
}
