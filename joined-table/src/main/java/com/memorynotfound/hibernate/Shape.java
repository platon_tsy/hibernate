package com.memorynotfound.hibernate;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Shape {

    @Id
    @Column(name = "shape_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

}
