package com.memorynotfound.hibernate;

import javax.persistence.*;
import java.util.List;

public class App {

    public static void main (String...args) throws InterruptedException {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mnf-pu");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Circle circle = new Circle(13.05);
        em.persist(circle);
        em.getTransaction().commit();

        em.getTransaction().begin();
        Rectangle rectangle = new Rectangle(5.02, 10.45);
        em.persist(rectangle);
        em.getTransaction().commit();

        List<Shape> accounts = em
                .createQuery( "select a from Shape a" )
                .getResultList();

        emf.close();
    }
}
