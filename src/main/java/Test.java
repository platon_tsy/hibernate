import com.model.Category;
import com.model.Product;
import com.util.HibernateUtil;
import org.hibernate.Session;

public class Test {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Product p = (Product) session.get(Product.class, 1L);
        System.out.println(p.getName());
        p.setName("Cookie");
        System.out.println(p.getClass().getName());
        session.getTransaction().commit();
        session.close();
        HibernateUtil.shutdown();
    }

}
