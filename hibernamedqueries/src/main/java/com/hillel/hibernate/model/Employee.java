package com.hillel.hibernate.model;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
@NamedNativeQueries({

		@NamedNativeQuery(name = "SQL_GET_ALL_EMPLOYEE", query = "select emp_id, emp_name, emp_salary from Employee"),
		@NamedNativeQuery(name = "SQL_GET_ALL_EMP_ADDRESS", query = "select e.*,a.* from Employee e join Address a ON e.emp_id=a.emp_id", resultClass = Employee.class )
})
@NamedQueries({
		@NamedQuery(name = "HQL_GET_ALL_EMPLOYEE", query = "from Employee"),
		@NamedQuery(name = "HQL_GET_EMPLOYEE_BY_ID", query = "from Employee where emp_id = :id"),
		@NamedQuery(name = "HQL_GET_EMPLOYEE_BY_SALARY", query = "from Employee where emp_salary > :salary")
})
@Entity
@Table(name = "Employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private long id;

	@Column(name = "emp_name")
	private String name;

	@Column(name = "emp_salary")
	private double salary;

	@OneToOne(mappedBy = "employee")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private Address address;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Id= " + id + ", Name= " + name + ", Salary= " + salary
				+ ", {Address= " + address + "}";
	}

}
