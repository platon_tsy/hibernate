package com.hillel.hibernate.main;

import com.hillel.hibernate.model.Person;
import com.hillel.hibernate.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class MappedSuperClassMain {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Session session = sessionFactory.openSession();

        session.beginTransaction();
        Person person = new Person();
        person.setName("petya");
        session.save(person);

        Person p = new Person();
        p.setName("Vasya");
        session.save(p);

        session.getTransaction().commit();

        session.flush();
        session.close();
        sessionFactory.close();
    }

}
