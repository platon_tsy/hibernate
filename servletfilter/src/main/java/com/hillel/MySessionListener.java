package com.hillel;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.logging.Logger;

@WebListener
public class MySessionListener implements HttpSessionListener {

    private static Logger log = Logger.getLogger(MySessionListener.class.getName());

    public void sessionCreated(HttpSessionEvent event) {

        log.info("A new session is created");
    }

    public void sessionDestroyed(HttpSessionEvent event) {

        log.info("session is destroyed");
    }

}
