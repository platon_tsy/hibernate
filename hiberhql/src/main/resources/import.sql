insert into Book (book_id, book_name, writer, price) VALUES(1, 'Java 8 in action', 'Taras Shevchenko', 100);
insert into Book (book_id, book_name, writer, price) VALUES (2, 'Javas Philosophy', 'Lesya Ukrainka', 120);
insert into Book (book_id, book_name, writer, price) VALUES (3, 'High performance java persistence', 'Pavlo Tychina', 140);
insert into Book (book_id, book_name, writer, price) VALUES (4, 'Linux command line', 'Alex Pushkin', 180);

INSERT INTO `Employee` (`emp_id`, `emp_name`, `emp_salary`) VALUES (1, 'Petya', 100);
INSERT INTO `Employee` (`emp_id`, `emp_name`, `emp_salary`) VALUES 	(2, 'Sasha', 200);
INSERT INTO `Employee` (`emp_id`, `emp_name`, `emp_salary`) VALUES (3, 'Vasya', 300);
INSERT INTO `Employee` (`emp_id`, `emp_name`, `emp_salary`) VALUES (4, 'Dasha', 400);


INSERT INTO `Address` (`emp_id`, `address_line1`, `zipcode`, `city`) VALUES (1, 'Velyka ave', '95129', 'Boryspil');
INSERT INTO `Address` (`emp_id`, `address_line1`, `zipcode`, `city`) VALUES 	(2, 'Mala ave', '95051', 'Brovary');
INSERT INTO `Address` (`emp_id`, `address_line1`, `zipcode`, `city`) VALUES 	(3, 'Petra Grygorovicha', '560100', 'Berduchiv');
INSERT INTO `Address` (`emp_id`, `address_line1`, `zipcode`, `city`) VALUES 	(4, 'City Centre', '100100', 'Kiev');