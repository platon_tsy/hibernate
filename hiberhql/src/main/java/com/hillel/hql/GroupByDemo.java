package com.hillel.hql;

import org.hibernate.Session;

import java.util.List;

public class GroupByDemo {
	public static void main(String[] args) {
		Session session = HibernateUtil.getHibernateSession();
		String hql = "select bk.writer, max(bk.price) from Book as bk group by bk.writer having avg(bk.price) > 100";
		List<?> list = session.createQuery(hql).list();
		for(int i=0; i<list.size(); i++) {
			Object[] row = (Object[]) list.get(i);
			System.out.println(row[0]+", "+ row[1]);
		}
		session.close();
	}
}
