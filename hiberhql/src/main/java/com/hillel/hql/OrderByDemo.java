package com.hillel.hql;

import org.hibernate.Session;

import java.util.List;

public class OrderByDemo {
	public static void main(String[] args) {
		Session session = HibernateUtil.getHibernateSession();
		String hql = "from Book as bk order by bk.bookName, bk.writer desc";
		List<?> list = session.createQuery(hql).list();
		for(int i=0; i<list.size(); i++) {
			Book book = (Book) list.get(i);
			System.out.println(book.getBookId()+", "+ book.getBookName()+
					","+book.getWriter()+", "+book.getPrice());
		}		
		session.close();
	}
}
